%--------------------------------------------------------------------------
%    examen TP G1
%--------------------------------------------------------------------------
close all; clear all; clc;
Nbits = 200; 
M = 2; 
Nbre_bit_sym = log2(M);

data_in = randi([0 1],Nbits,1);  
dataInMatrix = reshape(data_in,length(data_in)/Nbre_bit_sym,Nbre_bit_sym);   
dataSymbolsIn = bi2de(dataInMatrix);
Nfft=512;

EbNo =0 ; %dB
snrdB = EbNo + 10*log10(Nbre_bit_sym);
init_phase =0;  
sym = pskmod(dataSymbolsIn,M,init_phase); 
Db = 0.1; 
Tb = 1/Db; 
Fs = 1; 
Ts = 1/Fs;
Tsym = Nbre_bit_sym * Tb; 
Nsym = Nbits/Nbre_bit_sym;
temp = 0 : Ts : Nsym*Tsym;
fe = 0.2*1.01;
fc=0.2;
freq= -Fs/2:Fs/(Nfft-1):Fs/2;
%--------------------------------------------------------------------------

Seq_binaire = zeros(length(temp'),1)';  
Seq_binaire(1:Tb:Nbits*Tb)= data_in ;  
Seq_symbole = zeros(length(temp'),1)';  
Seq_symbole(1:Tsym:Nsym*Tsym)= sym ;  
s_i = filter(ones(1,Tsym),1,Seq_symbole);
s_passe_bande = real(s_i).* cos(2*pi*fc*temp)-imag(s_i).* sin(2*pi*fc*temp);


%--------------------------------------------------------------------------
r_passe_bande = awgn(s_passe_bande,snrdB,'measured');
%--------------------------------------------------------------------------

h=fliplr(g);
Rx_filter_real  = filter(ones(1,Tsym),1,r_passe_bande .* cos(2*pi*fc*temp));
Rx_filter_imag  = filter(ones(1,Tsym),1,r_passe_bande .* sin(2*pi*fc*temp));
Rx_filter = Rx_filter_real - 1i*Rx_filter_imag;
Rx_filter_sample = Rx_filter(Tsym:Tsym:length(Rx_filter))/(Tsym/2);
scatterplot(Rx_filter_sample);
sym_out = pskdemod(Rx_filter_sample,M,init_phase); 

data_out = reshape(de2bi(sym_out),[],1);
%--------------------------------------------------------------------------
[number_err,BER] = biterr(data_in,data_out);
%--------------------------------------------------------------------------






