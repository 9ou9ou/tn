clear all; close all; clc;
Ts = 1; 
Fs = 1/Ts;% sampling time/ freq
Fc = 0.1; 
Tb = 10; % bit period
Nsym = 10; 
M= 4; % Nbre de symbole 
Nb = Nsym*log2(M);  % Nb de bits !!! 
T = Tb*log2(M); % symbol period

time = 0:Ts:Tb*Nb-1;

seq_binaire = randi([0,1],1,Nb); % sequence binaire gen�r� aleatoirement
%bi2de: binary to decimale apr�s reshape
x = bi2de(reshape(seq_binaire,log2(M),Nsym)');
%modulation M-psk
if M==4
sym = pskmod(x,M,pi/4);
else
sym = pskmod(x,M,0);
end;
%scatter affiche la partie imaginaire en fct de la partie r�el
figure(2)
scatter(real(sym),imag(sym),'d','LineWidth',4)
grid on;
% Peigne de dirac * bits
alpha = zeros(1,(Tb*Nb)/Ts); % init
alpha(1:Tb:Tb/Ts*Nb)=seq_binaire;
% Peigne de dirac * symboles
a = zeros(1,(T*Nsym)/Ts); % init
a(1:T:(T*Nsym)/Ts) =sym;
% filtre de mise en forme Q1 c.
g = ones(1,T); % filtre fonction porte g(t)
xl= filter(g,1,a);

% Q1 d. Transposition en fr�quence signal s(t)
s=real(xl).*cos(2*pi*Fc*time) - imag(xl).*sin(2*pi*Fc*time);
%----------------------------------------------
figure(3)
subplot (3,1,1)
stem(time/T,real(a),':r','filled')
hold on 
plot(time/T,real(xl),'b','LineWidth',2)
grid on
legend('real(a(t))','real(s_l(t))')
subplot (3,1,2)
stem(time/T,imag(a),':r','filled')
hold on 
plot(time/T,imag(xl),'b','LineWidth',2)
grid on
legend('Imag(a(t))','Imag(s_l(t))')
subplot (3,1,3)
plot(time/T,s,'k-','LineWidth',2)
legend('s(t)')
xlabel('t/T')
grid on