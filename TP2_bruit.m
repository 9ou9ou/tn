clear all; close all; clc;
Ts = 1; Fs = 1/Ts;% sampling time/ freq
Fc = 0.1; 
Tb = 10; % bit period
Nsym = 1000; M= 8; % Nbre de symbole 
Nb = Nsym*log2(M);  % Nb de bits !!! 
T = Tb*log2(M); % symbol period

time = 0:Ts:Tb*Nb-1;

seq_binaire = randi([0,1],1,Nb);
x = bi2de(reshape(seq_binaire,log2(M),Nsym)');
sym = pskmod(x,M,0);

% Peigne de dirac * bits
alpha = zeros(1,(Tb*Nb)/Ts); % init
alpha(1:Tb:Tb/Ts*Nb)=seq_binaire;
% Peigne de dirac * symboles
a = zeros(1,(T*Nsym)/Ts); % init
a(1:T:(T*Nsym)/Ts) =sym;
% filtre de mise en forme Q1 c.
g = ones(1,T); % filtre fonction porte g(t)
xl= filter(g,1,a);

% Q1 d. Transposition en fr�quence signal s(t)
s=real(xl).*cos(2*pi*Fc*time) - imag(xl).*sin(2*pi*Fc*time);
%----------------------------------------------
%  Partie reception

%r=awgn(s,0,1);
r=s+0.7*randn(1,length(s));
h=(2/T)*g;
reel=r.*cos(2*pi*Fc*time);
imaginaire=r.*sin(2*pi*Fc*time)
y=filter(h,1,reel) -1i* (filter(h,1,imaginaire));

%h=fliplr(g)
%echantillonage
yech=y(T:T:length(y));
%Tracer les symboles
scatterplot(yech);
R_demod=pskdemod(yech,M);
d=de2bi(R_demod);
binaire=reshape(de2bi(R_demod')',1,Nb);
%compare 2 unsigned nbr and find nbr of error and there rate
[pe,error]= biterr(seq_binaire,binaire);
pe
error

% figure(2)
% seg1=awgn(zeros(1,T),0.001);
% [c,lags]=xcorr(seg1,50);
% stem(c,lags);