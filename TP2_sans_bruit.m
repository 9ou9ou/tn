clear all; close all; clc;
Ts = 1; 
Fs = 1/Ts;% sampling time/ freq
Fc = 0.1; 
%Fc=1.01*Fc;
Tb = 10; % bit period
Nsym = 10;
Nfft=512;
M= 4; % Nbre de symbole 
Nb = Nsym*log2(M);  % Nb de bits !!! 
T = Tb*log2(M); % symbol period
phi=0; %phase de la porteuse

time = 0:Ts:Tb*Nb-1;

seq_binaire = randi([0,1],1,Nb);
x = bi2de(reshape(seq_binaire,log2(M),Nsym)');
sym = pskmod(x,M,0);

% Peigne de dirac * bits
alpha = zeros(1,(Tb*Nb)/Ts); % init
alpha(1:Tb:Tb/Ts*Nb)=seq_binaire;
% Peigne de dirac * symboles
a = zeros(1,(T*Nsym)/Ts); % init
a(1:T:(T*Nsym)/Ts) =sym;
% filtre de mise en forme Q1 c.
g = ones(1,T); % filtre fonction porte g(t)
% filter permet de generer un filtre. elle prends en param�tre fct de
% filtre , 1 et la fct � filter
xl= filter(g,1,a);

% Q1 d. Transposition en fr�quence signal s(t)
s=real(xl).*cos(2*pi*Fc*time) - imag(xl).*sin(2*pi*Fc*time);
%----------------------------------------------

%  Partie reception

%r=awgn(s,0,1);
n=0;
r=s+n;
% h filtre adapt� h=g(T-t)
% 2/T facteur de normalisation
%h=(2/T)*g;
h=(2/T)*g
reel=r.*cos(2*pi*Fc*time+phi);
imaginaire=-1*r.*sin(2*pi*Fc*time+phi)
y=filter(h,1,reel) +1i* (filter(h,1,imaginaire));

%echantillonage
yech=y(T:T:length(y));
%demodulation
R_demod=pskdemod(yech,M);
%de2bi : changer de decimal au binaire
d=de2bi(R_demod);
binaire=reshape(de2bi(R_demod')',1,Nb);
% pour verifier  que le s�quence binaire au reception est que celle de l'emission 
seq_binaire-binaire
figure(1)
subplot(2,1,1);
plot(time/T,real(xl));
hold on
plot(time/T,real(y));

legend('real(xl)','real(y)');
subplot(2,1,2);
plot(time/T,imag(xl));
hold on
plot(time/T,imag(y));
legend('imag(xl)','imag(y)');

figure(2)
freq= -Fs/2:Fs/(Nfft-1):Fs/2;
DSPs=abs(fftshift(fft(s,Nfft))).^2;
DSPy=abs(fftshift(fft(y,Nfft))).^2;
hold on
grid on
semilogy(freq,DSPy);
semilogy(freq,DSPs);
legend('DSPsortieFiltre','DSPpasseBasEmis');