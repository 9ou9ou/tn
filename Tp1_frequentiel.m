clear all; close all; clc;
Ts = 1; 
Fs = 1/Ts;% sampling time/ freq
Fc = 0.3; 
Nfft=512;
Tb = 10; % bit period
Nsym = 100; 
M= 16; % Nbre de symbole 
Nb = Nsym*log2(M);  % Nb de bits !!! 
T = Tb*log2(M); % symbol period

% echelle de temps 
time = 0:Ts:Tb*Nb-1;
%echelle de frequence
freq= -Fs/2:Fs/(Nfft-1):Fs/2;
%on fait le boucle pour calculer la somme des xi puis on calcule la
%moyenne
for i=1:1:1000
seq_binaire = randi([0,1],1,Nb);
x = bi2de(reshape(seq_binaire,log2(M),Nsym)');
sym = qammod(x,M,0);
alpha = zeros(1,(Tb*Nb)/Ts); % init
alpha(1:Tb:Tb/Ts*Nb)=seq_binaire;
% Peigne de dirac * symboles
a = zeros(1,(T*Nsym)/Ts); % init
a(1:T:(T*Nsym)/Ts) =sym;
% filtre de mise en forme Q1 c.
g = ones(1,T); % filtre fonction porte g(t)
xl= filter(g,1,a);

% Q1 d. Transposition en fr�quence signal s(t)
s=real(xl).*cos(2*pi*Fc*time) - imag(xl).*sin(2*pi*Fc*time);
% fft transform� de fourier
%fftshift pour shifter ifft
DSPs(i,:)=abs(fftshift(fft(s,Nfft))).^2;
DSPa(i,:)=abs(fftshift(fft(a,Nfft))).^2;
DSPxl(i,:)=abs(fftshift(fft(xl,Nfft))).^2;
end
semilogy(freq,mean(DSPa));
% semilogy : tracage en echelle logaritmique
hold on
semilogy(freq,mean(DSPxl));
semilogy(freq,mean(DSPs));
legend('DSPa','DSPxl','DSPs');
grid on


